## [0.2.2] - 27-06-2017
### Update
- nginx to version 1.13.2 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.2.1] - 08-06-2017
### Update
- php version 7.0.20 available - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)

## [0.2.0] - 31-05-2017
### Update
- nginx to version 1.13.1 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.9] - 12-05-2017
### Update
- php package repository switched to [https://deb.sury.org/](https://deb.sury.org/)
- php version 7.0.19 available - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)

## [0.1.8] - 26-04-2017
### Update
- nginx to version 1.13.0 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.7] - 13-04-2017
### Update
- php version 7.0.18 available - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)
- nginx to version 1.11.13 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.6] - 21-03-2017
### Update
- Dockerfile reorganized
- README.md update
- php version 7.0.17 available - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)
- nginx to version 1.11.11 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.5] - 15-03-2017
### Update
- php version 7.0.16 available - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)
- nginx to version 1.11.10 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.4] - 14-12-2016
### Update
- php version 7.0.14 available - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)
- nginx to version 1.11.7 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.3] - 20-11-2016
### Update
- php to version 7.0.13 - [https://secure.php.net/ChangeLog-7.php](https://secure.php.net/ChangeLog-7.php)
- nginx to version 1.11.6 - [http://nginx.org/en/CHANGES](http://nginx.org/en/CHANGES)

## [0.1.2] - 18-11-2016
### Update
- php to version 7.0.12
- nginx to version 1.11.5

## [0.1.1] - 21-09-2016
### Update
- php to version 7.0.11

## [0.1.0] - 17-09-2016
### Update
- nginx to version 1.11.4

## [0.0.0] - 28-08-2016
### Added
- .dockerignore file
